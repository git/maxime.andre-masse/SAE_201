﻿using MusiLib.Model;

namespace MusiLib;

public partial class App : Application
{

    public Manager MyManager { get; private set; } = new Manager(new Stub.Stub());

    public string FileNameFU { get; set; } = "favorisUtilisateurs.xml";
    public string FileNameP { get; set; } = "partitions.xml";
    public string FilePath { get; set; } = FileSystem.AppDataDirectory;

    public App()
    {
        InitializeComponent();

        string favorisUtilisateursFilePath = Path.Combine(FilePath, FileNameFU);
        string partitionsFilePath = Path.Combine(FilePath, FileNameP);

        /*Si les fichiers existent déjà, on récupère la persistance car on a donc des favoris à récupérer*/
        if (File.Exists(partitionsFilePath) && File.Exists(favorisUtilisateursFilePath))
        {
            MyManager = new Manager(new DataContractPersistance.DataContractPers());
        }

        MyManager.chargeDonnees();

        MainPage = new AppShell();

        if (!File.Exists(partitionsFilePath) && !File.Exists(favorisUtilisateursFilePath))
        {
            MyManager.Persistance = new DataContractPersistance.DataContractPers();
        }

        MyManager.sauvegardeDonnees();

    }
}
