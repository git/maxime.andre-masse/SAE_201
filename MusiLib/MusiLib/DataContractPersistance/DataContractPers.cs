﻿using Microsoft.Maui.Controls.PlatformConfiguration;
using MusiLib.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MusiLib.DataContractPersistance
{
    public class DataContractPers : IPersistanceManager
    {
        public string FilePath { get; set; } = FileSystem.AppDataDirectory; /* FileSystem.AppDataDirectory permet de renvoyez le chemin d'accès au répertoire spécifique de donnée 
                                                                             * ou l'application peut stocker des fichiers ou des données persistantes*/
        public string FileNameFU { get; set; } = "favorisUtilisateurs.xml"; /* déclaration des noms des fichiers stockés pour la persistance*/
        public string FileNameP { get; set; } = "partitions.xml";

        public (List<Partition>, List<Partition>) chargeDonnees() /*Méthode permettant de charger les données*/
        {
            var partitionsSerializer = new DataContractSerializer(typeof(List<Partition>));
            var favorisUtilisateursSerializer = new DataContractSerializer(typeof(DataToPersist));

            List<Partition> partitions = new List<Partition>();
            List<Partition> favoris = new List<Partition>();

            using (Stream partitionsStream = File.OpenRead(Path.Combine(FilePath, "partitions.xml"))) /*Path.Combine permet de combiner plusieurs segments de chemin en une seule chaine de chemin*/
            {
                partitions = partitionsSerializer.ReadObject(partitionsStream) as List<Partition>; /*Serializer permet de convertir des objets */
            }

            using (Stream favorisUtilisateursStream = File.OpenRead(Path.Combine(FilePath, "favorisUtilisateurs.xml")))
            {
                DataToPersist data = favorisUtilisateursSerializer.ReadObject(favorisUtilisateursStream) as DataToPersist;
                favoris = data.favoris;
            }

            return (partitions,  favoris);
        }


        public void sauvegardeDonnees(List<Partition> p, List<Partition> f) /*Méthode permettant de sauvegarder les données*/
        {
            var partitionsSerializer = new DataContractSerializer(typeof(List<Partition>));
            var favorisUtilisateursSerializer = new DataContractSerializer(typeof(DataToPersist));

            if (!Directory.Exists(FilePath))
            {
                Debug.WriteLine("Directory créé à l'instant");
                Debug.WriteLine(Directory.GetDirectoryRoot(FilePath));
                Debug.WriteLine(FilePath);
                Directory.CreateDirectory(FilePath);
            }

            using (Stream partitionsStream = File.Create(Path.Combine(FilePath, FileNameP)))
            {
                partitionsSerializer.WriteObject(partitionsStream, p);
            }

            DataToPersist data = new DataToPersist();
            data.favoris = f;

            using (Stream favorisUtilisateursStream = File.Create(Path.Combine(FilePath, FileNameFU)))
            {
                favorisUtilisateursSerializer.WriteObject(favorisUtilisateursStream, data);
            }
        }

        public void sauvegardeFavoriAdd(Partition fav) /*Méthode permettant la sauvegarde de favoris*/
        {
            var favorisUtilisateursSerializer = new DataContractSerializer(typeof(DataToPersist));

            if (!Directory.Exists(FilePath))
            {
                Debug.WriteLine("Directory créé à l'instant");
                Debug.WriteLine(Directory.GetDirectoryRoot(FilePath));
                Debug.WriteLine(FilePath);
                Directory.CreateDirectory(FilePath);
            }

            List<Partition> favoris = new List<Partition>();
            using (Stream favorisUtilisateursStream = File.OpenRead(Path.Combine(FilePath, FileNameFU)))
            {
                DataToPersist data = favorisUtilisateursSerializer.ReadObject(favorisUtilisateursStream) as DataToPersist;
                if (data != null && data.favoris != null)
                {
                    favoris = data.favoris;
                }
            }

            favoris.Add(fav);

            using (Stream favorisUtilisateursStream = File.Create(Path.Combine(FilePath, FileNameFU)))
            {
                DataToPersist data = new DataToPersist();
                data.favoris = favoris;
                favorisUtilisateursSerializer.WriteObject(favorisUtilisateursStream, data);
            }

            Debug.WriteLine("Nouveau favori sauvegardé !");
        }

        public void sauvegardeFavoriRemove(Partition fav) /*Méthode permettant la sauvegarde de suppression de favoris */
        {
            var favorisUtilisateursSerializer = new DataContractSerializer(typeof(DataToPersist));

            if (!Directory.Exists(FilePath))
            {
                Debug.WriteLine("Directory créé à l'instant");
                Debug.WriteLine(Directory.GetDirectoryRoot(FilePath));
                Debug.WriteLine(FilePath);
                Directory.CreateDirectory(FilePath);
            }

            List<Partition> favoris = new List<Partition>();
            using (Stream favorisUtilisateursStream = File.OpenRead(Path.Combine(FilePath, FileNameFU)))
            {
                DataToPersist data = favorisUtilisateursSerializer.ReadObject(favorisUtilisateursStream) as DataToPersist;
                if (data != null && data.favoris != null)
                {
                    favoris = data.favoris;
                }
            }

            favoris.RemoveAll(f => f.Nom == fav.Nom);

            using (Stream favorisUtilisateursStream = File.Create(Path.Combine(FilePath, FileNameFU)))
            {
                DataToPersist data = new DataToPersist();
                data.favoris = favoris;
                favorisUtilisateursSerializer.WriteObject(favorisUtilisateursStream, data);
            }

            Debug.WriteLine("Favori supprimé de la sauvegarde !");
        }


    }
}