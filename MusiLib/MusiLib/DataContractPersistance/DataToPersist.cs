﻿using MusiLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiLib.DataContractPersistance
{
    public class DataToPersist
    {
        public List<Partition> partitions { get; set; } = new List<Partition>();
        public List<Partition> favoris { get; set; } = new List<Partition>();
    }
}
