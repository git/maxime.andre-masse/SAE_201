﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiLib.Model
{
    public interface IPersistanceManager
    {
        (List<Partition>, List<Partition>) chargeDonnees();

        /*déclaration de fonctions abstraites*/
        void sauvegardeDonnees(List<Partition> p,  List<Partition> f);  

        void sauvegardeFavoriAdd(Partition fav);

        void sauvegardeFavoriRemove(Partition fav);
    }
}
