﻿using MusiLib.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiLib.Model
{
    public class Manager
    {
        public IPersistanceManager Persistance { get; set; }
        public List<Partition> partitions {  get; private set; }
        public List<Partition> favoris { get; private set; }

        public Manager() 
        {
            partitions = new List<Partition>();
            favoris = new List<Partition>();
        }

        public Manager(IPersistanceManager persistance)
        {
            Persistance = persistance;
            partitions = new List<Partition>();
            favoris = new List<Partition>();
        }

        public void ajouterPartition(Partition partition)
        {
            partitions.Add(partition);
        }

        public void chargeDonnees()
        {
            var donnees = Persistance.chargeDonnees();   /*variable local, permet d'éviter la répétition d'un nom de type dans une déclaration de variable */
            partitions.AddRange(donnees.Item1);
            favoris.AddRange(donnees.Item2);
        }

        public void sauvegardeDonnees() /*Sauvegarde les partitions et favoris*/
        {
            Persistance.sauvegardeDonnees(partitions, favoris);
        }

        public void sauvegardeFavoriAdd(Partition fav) /*Sauvegarde des Favoris*/
        {
            Persistance.sauvegardeFavoriAdd(fav);
        }

        public void sauvegardeFavoriRemove(Partition fav) /*Suppression d'une partition contenu dans les Favoris*/
        {
            Persistance.sauvegardeFavoriRemove(fav);
        }

    }
}


