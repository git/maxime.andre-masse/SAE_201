﻿using Plugin.Maui.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Maui.Controls;


namespace MusiLib.Model
{
    public class Metronome
    {
        public AudioPlayer player;
        public bool isMusicPlaying;
        public bool isMusicBeginning;

        public Metronome()
        {
            player = DependencyService.Get<AudioPlayer>(); /* DependencyService = méthode utilisée pour obtenir une instance d'un service audio personnalisé.*/
            isMusicPlaying = false;
        }

        public async Task Lancer(string cheminFichier)
        {
                var fichierAudio = (AudioPlayer)AudioManager.Current.CreatePlayer(await FileSystem.OpenAppPackageFileAsync(cheminFichier)); /*Créer un lecteur Audio à partir d'un fichier se trouvant dans le package de l'application*/
                player = fichierAudio;
                player.Play(); /*Méthode permettant de lancer la musique venant de Plugin.Maui.Audio */
                isMusicPlaying=true;
                isMusicBeginning =true;
        }
        public void PlayMusic() /*Méthode permettant de jouer/relancer la musique*/
        {
            player.Play();
            isMusicPlaying = true;
        }
        public void PauseMusic() /*Méthode permettant la mise en pause de la musique*/
        {
            player.Pause();
            isMusicPlaying = false;
        }
        public void StopMusic() /*Méthode permettant de stopper la lecture de la musique*/
        {
            if (isMusicBeginning)
            {
                player.Stop(); /*Méthode permettant de stopper la musique venant de Plugin.Maui.Audio*/
                isMusicPlaying = false;
                isMusicBeginning = false;
            }
        }
        public void ReglerTempo(float tempo) /*Méthode permettant de regler la vitesse d'un objet de classe méttronome*/
        {
            if(!isMusicBeginning)
            {
                return;
            }
            if(!isMusicPlaying)
            {
                player.Speed = tempo; /*Méthode de Plugin.maui.Audio permettant de régler la vitesse d'un audio/vidéo*/
                player.Pause();
                return;
            }
            player.Speed = tempo;
        }
    }

}

