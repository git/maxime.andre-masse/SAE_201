﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MusiLib.Model
{
    [DataContract]
    public class Partition
    {
        [DataMember]
        public string Nom { get; private set; }
        [DataMember]
        public List<string> Image { get; private set; }
        [DataMember]
        public int IdPartition { get; private set; }
        [DataMember]
        public string Auteur { get; private set; }
        [DataMember]
        public string Complexite { get; private set; }
        [DataMember]
        public string Description { get; private set; }
        [DataMember]
        public string Instrument { get; private set; }
        [DataMember]
        public string Son { get; private set; }
  
        public Partition(string nom, string auteur, string complexite, string description, string instrument, string son)
        {
            Nom = nom;
            Auteur = auteur;
            Complexite = complexite;
            Description = description;
            Instrument = instrument;
            Image = new List<string>();
            Son = son;
        }
        public void ajouterImage(string image)
        {
            Image.Add(image);
        }
        public void supprimerImage(string image)
        {
            Image.Remove(image);
        }

    }
}
