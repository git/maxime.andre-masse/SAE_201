﻿using System;
using System.Collections.Generic;
using System.Linq;
using MusiLib.Model;

namespace MusiLib.Model
{
    public class Trier
    {
        private List<Partition> partitionsInitiales;
        private List<Partition> partitionsFiltrees;

        public Trier(List<Partition> partitions)
        {
            partitionsInitiales = partitions;
            partitionsFiltrees = new List<Partition>(partitionsInitiales);
        }

        public List<Partition> TrierParInstrument(string instrument) /*Méthode permettant de trier les partitions par instrument*/
        {
            partitionsFiltrees = partitionsInitiales.Where(p => p.Instrument.ToLower() == instrument.ToLower()).ToList(); /*ToLower permet la convertion d'une chaine de caractères en minuscules*/
            return partitionsFiltrees;
        }

        public List<Partition> TrierParComplexite(string complexite) /*Méthode permettant de trier les partitions par leur complexité (difficulté à les jouer)*/
        {
            partitionsFiltrees = partitionsInitiales.Where(p => p.Complexite.ToLower() == complexite.ToLower()).ToList();
            Console.WriteLine("string : " +  complexite);
            foreach (Partition partition in partitionsInitiales)
            {
                Console.WriteLine(partition.Complexite);
            }
            return partitionsFiltrees;
        }

        public List<Partition> TrierParOrdreAlphabetique(string ordre) /*Méthode permetttant de trier les partitions par l'ordre alphabétique de leur Nom*/
        {
            if (ordre == "Croissant")
                partitionsFiltrees = partitionsInitiales.OrderBy(p => p.Nom).ToList();
            else if (ordre == "Décroissant")
                partitionsFiltrees = partitionsInitiales.OrderByDescending(p => p.Nom).ToList();

            return partitionsFiltrees;
        }
    }
}
