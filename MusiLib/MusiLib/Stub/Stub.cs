﻿using Microsoft.Maui.Controls;
using MusiLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace MusiLib.Stub
{
    public class Stub : IPersistanceManager
    {
        public (List<Partition>, List<Partition>) chargeDonnees()
        {
            List<Partition> partitions = new List<Partition>();
            
            List<Partition> favoris = new List<Partition>();


            /*Charge toutes nos partitions*/

            Partition amazing_grace = new Partition("Amazing Grace", "John Newton", "Facile", "Amazing Grace est l'un des cantiques chrétiens les plus célèbres dans le monde anglophone. La première publication des paroles date de 1779. Associé à diverses mélodies au fil des années, il est aujourd'hui interprété sur la musique de New Britain", "piano",  "amazing-grace.mid");
            Partition au_clair_de_la_lune = new Partition("Au clair de la lune", "Édouard-Léon Scott de Martinville", "Moyen", "Au clair de la lune est une chanson populaire française dont la mélodie, très caractéristique, ainsi que les paroles — surtout celles du premier couplet — sont si familières qu'elles ont fait l'objet d'innombrables citations, adaptations, parodies, pastiches, etc.", "piano",  "au_clair_de_la_lune.wav");
            Partition aura_lee_love_me = new Partition("aura lee love me", "Elvis Presley", "Facile", " Love Me Tender est une ballade de 1956 enregistrée par Elvis Presley et publiée par  Elvis Presley Music du film du même nom de la 20th Century Fox . Les paroles sont créditées à Vera Matson (bien que le parolier réel était son mari, Ken Darby) et Elvis Presley lui-même. La mélodie est identique à la ballade sentimentale de la guerre civile Aura Lea et donc attribuée au compositeur d'Aura Lea, l'Anglais George R. Poulton.", "piano",  "aura-lee-love-me-tender.mid");
            Partition ode_a_la_joie = new Partition("Hymne à la joie", "Friedrich von Schiller", "Difficile", "Ode à la joie — appelée également Hymne à la joie1 — est un poème de Friedrich von Schiller écrit en 1785. Il est surtout connu comme finale du quatrième et dernier mouvement de la 9e Symphonie de Beethoven, devenu l'hymne officiel de l'Union européenne2.Ce poème célèbre l'idéal de l'unité et de la fraternité humaines (« Millions d’êtres, soyez tous embrassés d’une commune étreinte ! ). Son titre original est An die Freude, mais il est souvent appelé Ode an die Freude.", "piano", "amazing-grace.mid");
            Partition fais_dodo_colas = new Partition("Fais dodo", "Inconnu", "Facile", "Fais dodo est une berceuse enfantine en langue française dont l'auteur est inconnu et est dans le domaine public. La mélodie, très connue, date du XVIIIe siècle et se chante dans toutes les régions de France et également au Québec. Il y a quelques variantes, où les ingrédients du texte changent : nougat au lieu de chocolat, d'autres fois des bateaux : « Papa est en haut qui fait des bateaux pour le p'tit Pierrot qui fait son dodo... » ", "piano", "fais-dodo-colas-mon-petit-frere.mid");
            Partition Frere_Jacques = new Partition("Frère Jacques", "Jean-Philippe Rameau", "Moyen", "Frère Jacques est une chanson enfantine française du XVIIIe siècle, connue dans le monde entier et traduite dans de nombreuses langues. Longtemps considérée comme anonyme, elle a vraisemblablement pour auteur Jean-Philippe Rameau. ", "piano", "frere-jacques.mid");
            Partition march = new Partition("March", "Jean-Sébastien Bach", "Difficile", "Jean-Sébastien Bach, né à Eisenach (Duché de Saxe-Eisenach) le 21 mars 1685 et mort à Leipzig le 28 juillet 1750, est un compositeur, musicien, et notamment organiste, allemand.", "violon", "march.mp3");
            Partition partita_minor = new Partition("Partita in a Minor", "Jean-Sébastien Bach", "Intermédiaire", "La partita pour flûte traversière seule (ou Sonate suivant les éditions) en la mineur BWV 1013 de Jean-Sébastien Bach, seule œuvre pour cet instrument sans accompagnement de ce compositeur, a été éditée une première fois en 1917 d'après un manuscrit du xviiie siècle Solo pour une flûte traverso par Jean-Sébastien Bach, mais nous n'avons pas d'indication sur les circonstances de sa composition.", "flûte", "partita-minor.mp3");
            Partition bach_prelude = new Partition("Bach Johann Prélude","Jean-Sébastien Bach", "Intermédiaire", "Jean-Sébastien Bach [ ʒɑ̃sebastjɛ̃ bak]1 (en allemand : Johann Sebastian Bach [ˈjoːhan zeˈbasti̯an baχ]2 Écouter), né à Eisenach (Duché de Saxe-Eisenach) le 21 mars 1685 (31 mars 1685 dans le calendrier grégorien) et mort à Leipzig le 28 juillet 1750, est un compositeur, musicien, et notamment organiste, allemand.","flûte","bach-johann-prelude.mp3");
            Partition Beau = new Partition("Beau","Guy Bergeron","Intermédiaire", "Guy Bergeron, né le 31 août 19641, est un écrivain québécois spécialisé en fantastique et fantasy. Son premier roman, L'Orbe et le Croissant, est publié par les Éditions Arion en 2006.","flute","guy-beau.mp3");
            Partition a_cadence = new Partition("a_cadence","Roméo Caldini","Difficile", "Diplômé de l’école secondaire du conservatoire de musique de Florence en 1977, il se perfectionne en piano, puis suit des cours de musique de chambre. Il étudie l'harmonie et le contrepoint ainsi que le clavecin. Il a enseigné aux conservatoires de Florence, Cosenza et Vibo Valentia, au lycée de musique d’ Arezzo et à l'institut musical de Modène. Il enseigne également la musique de chambre au conservatoire de Milan.","flûte","caldini-a-cadence.mp3");
            Partition alleluia = new Partition("alléluia","Inconnu","Facile", "L'Alléluia est une acclamation de louange envers Dieu qui se trouve dans la Bible hébraïque et a été réutilisée par la liturgie chrétienne. Le mot Alleluia ou Hallelujah (en hébreu : הַלְּלוּיָהּ, transcrit ἀλληλούϊα / hallêloúïa en grec), signifie littéralement « louez Yah » (hallelu-Yah).","violon","thode-violon-bleu.midi");
            Partition WestWorld = new Partition("WestWorld","Ramin Djawadi","Intermédiaire","Westworld: Season 1 est la première bande originale de la série télévisée américaine Westworld, composée par Ramin Djawadi. Sorti le 5 décembre 2016, l’album comprend trente-quatre pièces composées ou arrangées pour le spectacle. [1] L’album contient principalement des compositions originales de Djawadi, il comprend également des reprises de Radiohead, The Rolling Stones, Soundgarden, The Animals et The Cure[2]. [3] La bande originale a reçu des critiques favorables et a culminé à #190 sur le US Billboard 200 chart.","piano","WestWorld.mp3");

            amazing_grace.ajouterImage("amazing.png");
            au_clair_de_la_lune.ajouterImage("au_clair.png");
            aura_lee_love_me.ajouterImage("aura_lee_love_me.png");
            ode_a_la_joie.ajouterImage("ode_a_la.png");
            fais_dodo_colas.ajouterImage("fais_dodo_colas.png");
            Frere_Jacques.ajouterImage("frere_jacques.png");
            march.ajouterImage("march.png");
            partita_minor.ajouterImage("partita_minor_1.png");
            partita_minor.ajouterImage("partita_minor_2.png");
            partita_minor.ajouterImage("partita_minor_3.png");
            partita_minor.ajouterImage("partita_minor_4.png");
            partita_minor.ajouterImage("partita_minor_5.png");
            bach_prelude.ajouterImage("bach_johann_prelude.png");
            bach_prelude.ajouterImage("bach_johann_prelude2.png");
            Beau.ajouterImage("guy_beau.png");
            Beau.ajouterImage("guy_beau2.png");
            Beau.ajouterImage("guy_beau3.png");
            a_cadence.ajouterImage("caldini_a_cadence.png");
            a_cadence.ajouterImage("caldini_a_cadence2.png");
            alleluia.ajouterImage("thode_violon_bleu.png");
            WestWorld.ajouterImage("westworld.png");

            partitions.Add(amazing_grace);
            partitions.Add(au_clair_de_la_lune);
            partitions.Add(aura_lee_love_me);
            partitions.Add(ode_a_la_joie);
            partitions.Add(fais_dodo_colas);
            partitions.Add(Frere_Jacques);
            partitions.Add(march);
            partitions.Add(partita_minor);
            partitions.Add(bach_prelude);
            partitions.Add(Beau);
            partitions.Add(a_cadence);
            partitions.Add(alleluia);
            partitions.Add(WestWorld);

            return (partitions, favoris);
        }

        public void sauvegardeDonnees(List<Partition> p, List<Partition> f)
        {
            
        }

        public void sauvegardeFavoriAdd(Partition fav)
        {

        }

        public void sauvegardeFavoriRemove(Partition fav)
        {

        }
    }
}
