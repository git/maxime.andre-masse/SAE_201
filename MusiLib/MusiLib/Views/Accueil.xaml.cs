using Plugin.Maui.Audio;
using MusiLib.Model;

namespace MusiLib.Views
{
    public partial class Accueil : ContentPage, IAllowClick
    {
        public Manager MyManager => (App.Current as App).MyManager;

        private List<Partition> partitionsInitiales;
        private List<Partition> partitionsFiltrees;
        private Trier trieur;

        public Accueil()
        {
            InitializeComponent();
            partitionsInitiales = MyManager.partitions.ToList();
            partitionsFiltrees = new List<Partition>(partitionsInitiales);
            trieur = new Trier(MyManager.partitions.ToList());
            BindingContext = MyManager;
            chargerPartitions();
        }

        /*Nous redirige vers la page des favoris*/
        private void GoToFavorisButton(object sender, EventArgs e)
        {
            if (!IAllowClick.AllowTap) return;
            else IAllowClick.AllowTap = false;

            Navigation.PushAsync(new Favoris());

            IAllowClick.ResumeTap();
        }

        /*Nous redirige vers la partition sur laquelle on a cliqu�*/
        private void GoToPartitionButton(object sender, EventArgs e)
        {
            if (!IAllowClick.AllowTap) return;
            else IAllowClick.AllowTap = false;

            var button = (ImageButton)sender;
            var idAutomation = button.AutomationId;

            if (int.TryParse(idAutomation, out int id))
            {
                Navigation.PushAsync(new PartitionView(id));
            }

            IAllowClick.ResumeTap();
        }

        /*Charge automatiquement toutes les partitions en ImageButton et les affiche � l'�cran*/
        private void chargerPartitions()
        {
            /*Supprime les partitions pr�sentes (on les diff�rencie des autres ImageButton gr�ce � l'automationId*/
            var imageButtonsToRemove = grille.Children.OfType<ImageButton>().Where(btn => !string.IsNullOrEmpty(btn.AutomationId)).ToList();
            foreach (var button in imageButtonsToRemove)
            {
                grille.Children.Remove(button);
            }


            int imagesParLigne = 3;
            int indice = 0;

            /*Cr�e les ImageButton des partitions*/
            foreach (Partition partition in partitionsFiltrees)
            {
                int indicePartition = MyManager.partitions.IndexOf(partition);

                ImageButton imageButton = new ImageButton
                {
                    Source = partition.Image[0],
                    WidthRequest = 175,
                    HeightRequest = 175,
                    AutomationId = indicePartition.ToString(),
                };

                imageButton.Clicked += GoToPartitionButton;

                int ligne = 1 + (indice / imagesParLigne);
                int colonne = indice % imagesParLigne;

                imageButton.Margin = GetImageButtonMargin(colonne);

                Grid.SetRow(imageButton, ligne);
                Grid.SetColumn(imageButton, colonne);
                grille.Children.Add(imageButton);

                indice++;
            }
        }

        /*R�cup�re des valeurs pr�d�finies pour les margin de chargerFavoris afin d'avoir un affichage align�*/
        private Thickness GetImageButtonMargin(int colonne)
        {
            if (colonne == 0)
            {
                return new Thickness(30, 0, 0, 0);
            }
            else if (colonne == 1)
            {
                return new Thickness(90, 0, 0, 0);
            }
            else
            {
                return new Thickness(150, 0, 0, 0);
            }
        }

        /*Permet de modifier l'affichage des partitions en fonction de ce qu'on recherche*/
        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            string texteRecherche = e.NewTextValue;

            partitionsFiltrees.Clear();

            foreach (Partition partition in partitionsInitiales)
            {
                if (partition.Nom.ToLower().Contains(texteRecherche.ToLower()))
                {
                    partitionsFiltrees.Add(partition);
                }
            }

            chargerPartitions();
        }

        /*Lance un popup de plusieurs choix quand on clic sur le bouton de tri*/
        private async void TriButton_Clicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Trier par", "Annuler", null, "Type", "Difficult�", "Ordre alphab�tique", "R�initialiser");

            switch (action)
            {
                case "Type":
                    await TrierParTypeButton();
                    break;
                case "Difficult�":
                    await TrierParDifficulteButton();
                    break;
                case "Ordre alphab�tique":
                    await TrierParOrdreAlphabetiqueButton();
                    break;
                case "R�initialiser":
                    ReinitialiserButton();
                    break;
            }

        }

        /*Lance un popup de plusieurs choix quand on clic sur Type dans le popup TriButton_Clicked*/
        private async Task TrierParTypeButton()
        {
            var action = await DisplayActionSheet("Trier par", "Annuler", null, "Fl�te", "Piano", "Violon");

            if (action == "Annuler")
                return;

            var partitionsTriees = trieur.TrierParInstrument(action);
            partitionsFiltrees = new List<Partition>(partitionsTriees);
            chargerPartitions();
        }

        /*Lance un popup de plusieurs choix quand on clic sur Difficult� dans le popup TriButton_Clicked*/
        private async Task TrierParDifficulteButton()
        {
            var action = await DisplayActionSheet("Trier par", "Annuler", null, "Facile", "Moyen", "Difficile");

            if (action == "Annuler")
                return;

            var partitionsTriees = trieur.TrierParComplexite(action);
            partitionsFiltrees = new List<Partition>(partitionsTriees);
            chargerPartitions();
        }

        /*Lance un popup de plusieurs choix quand on clic sur Ordre Alphab�tique dans le popup TriButton_Clicked*/
        private async Task TrierParOrdreAlphabetiqueButton()
        {
            var action = await DisplayActionSheet("Trier par", "Annuler", null, "Croissant", "D�croissant");

            if (action == "Annuler")
                return;

            var partitionsTriees = trieur.TrierParOrdreAlphabetique(action);
            partitionsFiltrees = new List<Partition>(partitionsTriees);
            chargerPartitions();
        }

        /*R�initialise les partitions pour annuler le tri*/
        private void ReinitialiserButton()
        {
            partitionsFiltrees.Clear();
            partitionsFiltrees.AddRange(partitionsInitiales);
            chargerPartitions();
        }

    }
}
