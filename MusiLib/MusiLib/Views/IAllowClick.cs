﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiLib.Views
{
    /*Permet de mettre un délai entre chaque clic pour éviter les problèmes de multi-pages sur Android*/
    /*Est utilisé sur chaque bouton sous la forme :
     
            if (!IAllowClick.AllowTap) return;
            else IAllowClick.AllowTap = false;

            ......

            IAllowClick.ResumeTap();
    */
    public interface IAllowClick
    {
        public static bool AllowTap = true;

        public static async void ResumeTap()
        {
            await Task.Delay(500);
            AllowTap = true;
        }
    }
}
