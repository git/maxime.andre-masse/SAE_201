using Microsoft.Maui.Controls.PlatformConfiguration;
using MusiLib.Model;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace MusiLib.Views;


public partial class PartitionView : ContentPage
{

    public Manager MyManager => (App.Current as App).MyManager;

    public Metronome music = new Metronome();
    public Metronome metronome = new Metronome();

    public string NomPartitionTitle { get; set; }
    public int IdTab { get; set; }


    public PartitionView()
	{
		InitializeComponent();
        BindingContext = MyManager;
    }

    public PartitionView(int id)
    {
        InitializeComponent();
        IdTab = id;
        BindingContext = this;
        NomPartitionTitle = MyManager.partitions[IdTab].Nom;
        Part.BindingContext = MyManager.partitions[IdTab];

        InitializeButton();
        ChargerPartitionsSimilaires();
    }

    /*Permet de lancer la musique de la partition en cours*/
    private void Play_Music(object sender, EventArgs e)
    {

        Button button = (Button)sender;
        if (!music.isMusicBeginning)
        {
            _ = music.Lancer(MyManager.partitions[IdTab].Son);
            Slider sliderMusic = (Slider)FindByName("tempo_slider");
            music.ReglerTempo((float)sliderMusic.Value);
            button.Text = "Pause";
        }
        else if(!music.isMusicPlaying)
        {
            music.PlayMusic();
            button.Text = "Pause";
        }
        else
        {
            music.PauseMusic();
            button.Text = "Jouer";
        }
    }

    /*Permet de lancer le m�tronome*/
    private void Play_Metronome(object sender, EventArgs e)
    {

        Button button = (Button)sender;
        if (!metronome.isMusicBeginning)
        {
            _ = metronome.Lancer("40_BPM_Metronome.mp3");
            Slider sliderMetro = (Slider)FindByName("BPM_slider");
            metronome.ReglerTempo((float) sliderMetro.Value);
            button.Text = "Pause";
        }
        else if (!metronome.isMusicPlaying)
        {
            metronome.PlayMusic();
            button.Text = "Pause";
        }
        else
        {
            metronome.PauseMusic();
            button.Text = "Jouer";
        }
    }

    /*Permet d'arr�ter la musique*/
    private void Stop_Music(object sender, EventArgs e)
    {
        music.StopMusic();
        Slider sliderMusic = (Slider)FindByName("tempo_slider");
        Slider sliderMetro = (Slider)FindByName("BPM_slider");
        Button button = (Button)FindByName("play_music_button");
        button.Text = "Jouer";
        sliderMusic.Value = 1;
        sliderMetro.Value = 1;
    }

    /*Permet d'arr�ter le m�tronome*/
    private void Stop_Metronome(object sender, EventArgs e)
    {
        metronome.StopMusic();
        Button button = (Button)FindByName("play_metronome_button");
        button.Text = "Jouer";
    }

    /*Coupe la musique et le m�tronome quand on revient en arri�re avec le bouton retour du t�l�phone*/
    protected override bool OnBackButtonPressed()
    {
        metronome.StopMusic();
        music.StopMusic();
        return base.OnBackButtonPressed();
    }

    /*Coupe la musique et le m�tronome quand on revient en arri�re*/
    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        metronome.StopMusic();
        music.StopMusic();
    }

    /*Initialise le Shell_Navigating au chargement de la page*/
    protected override void OnAppearing()
    {
        base.OnAppearing();
        Shell.Current.Navigating += Shell_Navigating;
    }

    /*Coupe la musique et le m�tronome quand on revient en arri�re avec le bouton retour de l'appli | Fonctionne avec OnDisappearing*/
    private void Shell_Navigating(object sender, ShellNavigatingEventArgs e)
    {
        if (e.Source == ShellNavigationSource.ShellItemChanged)
        {
            metronome.StopMusic();
            music.StopMusic();
        }
    }

    /*Permet de mettre la bonne image suivant si la partition est en favori ou pas au chargement*/
    private void InitializeButton()
    {
        if (MyManager.favoris.Any(favori => favori.Nom == MyManager.partitions[IdTab].Nom))
        {
            favoriButton.Source = "etoile.png";
        }
        else
        {
            favoriButton.Source = "etoile_vide.png";
        }
    }

    /*Initialise et cr�e des ImageButton des partitions du m�me auteur et m�me instrument pour les afficher automatiquement*/
    private void ChargerPartitionsSimilaires()
    {
        string auteur = MyManager.partitions[IdTab].Auteur;
        string instrument = MyManager.partitions[IdTab].Instrument;

        var partitionsSimilaires = MyManager.partitions.Where(p => p.Auteur.ToLower() == auteur.ToLower() ||
            p.Instrument.ToLower() == instrument.ToLower()).ToList();

        uint i = 1;

        foreach (var partition in partitionsSimilaires)
        {
            ++i;
            if(i>9)
            {
                return;
            }
            int indicePartition = MyManager.partitions.IndexOf(partition);

            var imageButton = new ImageButton
            {
                Source = partition.Image[0],
                HeightRequest = 80,
                WidthRequest = 80,
                AutomationId = indicePartition.ToString()
            };

            imageButton.Clicked += GoToPartitionButton;

            PartitionsSimilairesStackLayout.Children.Add(imageButton);
        }
    }



    /*Nous redirige vers la partition sur laquelle on a cliqu�*/
    private void GoToPartitionButton(object sender, EventArgs e)
    {
        if (!IAllowClick.AllowTap) return;
        else IAllowClick.AllowTap = false;

        var button = (ImageButton)sender;
        var idAutomation = button.AutomationId;

        if (int.TryParse(idAutomation, out int id))
        {
            Navigation.PushAsync(new PartitionView(id));
        }

        IAllowClick.ResumeTap();
    }

    /*Permet d'ajouter une partition aux favoris, sauvegarde sur le t�l�phone et change l'image du bouton*/
    private void AddFavoriButton(object sender, EventArgs e)
    {
        ImageButton button = (ImageButton)sender;

        if (!MyManager.favoris.Any(favori => favori.Nom == MyManager.partitions[IdTab].Nom))
        {
            MyManager.favoris.Add(MyManager.partitions[IdTab]);
            MyManager.sauvegardeFavoriAdd(MyManager.partitions[IdTab]);
            button.Source = "etoile.png";
        }
       else
        {
            MyManager.favoris.RemoveAll(f => f.Nom == MyManager.partitions[IdTab].Nom);
            MyManager.sauvegardeFavoriRemove(MyManager.partitions[IdTab]);
            button.Source = "etoile_vide.png";
        }
    }

    /*Permet d'envoyer la valeur du slider pour modifier le tempo de la musique*/
    private void TempoSlider(object sender, ValueChangedEventArgs e)
    {
        float selectedValue = (float)e.NewValue;
        music.ReglerTempo(selectedValue);
    }

    /*Permet d'envoyer la valeur du slider pour modifier le tempo du m�tronome*/
    private void BPMSlider(object sender, ValueChangedEventArgs e)
    {
        float selectedValue = (float)e.NewValue;
        metronome.ReglerTempo(selectedValue);
    }

}
